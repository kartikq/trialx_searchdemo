#!/usr/bin/env python
import pysolr
import requests
import csv

def main():
    states = parse_states()
    for abbreviation in states.keys():
        trial_info = get_trial_info(abbreviation)['ClinicalTrials']
        for trial in trial_info:
            add_to_solr(trial, states)

def parse_states():
    reader = csv.reader(open('state_table.csv'), delimiter=',', quotechar='"')
    return dict([(row[1],row[0]) for row in reader])

def get_trial_info(state):
    return requests.get('http://trialx.com/query', params={ 'op':'json','clocation':state,'start':'11'}).json()

def add_to_solr(doc, state_map):
    solr = pysolr.Solr('http://localhost:8080/solr/', timeout=10)
    solr_docs = []
    for site in doc['Sites']:
        solr_docs.append({'trial_id_key':doc['Id'], 'condition':doc['Condition']
            , 'title':doc['Title'], 'citykw':site['city'], 'statekw':site['state'],'state':[site['state'],state_map[site['state']]]
            , 'sitename':site['sitename'], 'address':site['address'], 'zipkw':site['zip'], 'studytypekw':doc['StudyType'], 'phasekw':doc['Phase']})
    solr.add(solr_docs)
    print "Added doc id " + doc['Id']

if __name__ == '__main__':
    main()
