from django.shortcuts import render
import json
import jsonpickle
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.template.response import TemplateResponse
from solr_client import SolrClient

FACET_DISPLAY = {'phasekw':'Phase', 'studytypekw':'Study type'}

def query_solr(request):
    form = request.GET.copy()
    if 'search' not in form:
        return TemplateResponse(request, 'search.html')

    query_text = form['search']
    selected_facets = form['selected_facets']

    fqs = ' AND '.join(filter(None, selected_facets.split('|')))
    solr_params = {'facet':'true','facet.field':['phasekw','studytypekw']
            , 'facet.mincount':'1','fq': fqs}
    solr_results = SolrClient().query(query_text, solr_params)

    docs = []
    for doc in solr_results.docs:
        docs.append(dict([(k,",".join(v)) if type(v)==list else (k,v) for k,v in doc.iteritems()]))
    facets = dict([(k,{'name': FACET_DISPLAY[k], 'values': dict(zip(*[iter(v)]*2)) } ) 
        for k,v in solr_results.facets['facet_fields'].iteritems()])

    response = {'docs': docs, 'hits': solr_results.hits
            , 'facets': facets, 'query': query_text}
    return TemplateResponse(request, 'results.html', response)

def file_upload(request):
    response = {}
    if request.method == 'POST':
        handle_uploaded_file(request.FILES['file'])
        response = {'uploaded': 'true'}
    return TemplateResponse(request, 'files.html', response)

def handle_uploaded_file(f):
    fname = '/tmp/' + f.name
    with open(fname, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

class SearchView(TemplateView):
    template_name = 'search.html'
