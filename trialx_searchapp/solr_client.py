import pysolr
from django.conf import settings

class SolrClient(object):
    def __init__(self):
        self.solr = pysolr.Solr(settings.SOLR_URL, timeout=10)

    def query(self, query, params={}):
        return self.solr.search(query, **params)
