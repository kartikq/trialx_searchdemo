from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import views

urlpatterns = patterns('views',
    url(r'^$', views.SearchView.as_view(),name='home'),
    url(r'^query',views.query_solr, name='query'),
    url(r'^file',views.file_upload, name='file')
)

#remove this in production and use a web server to serves statics
urlpatterns += staticfiles_urlpatterns()
