<?xml version="1.0" encoding="UTF-8" ?>
<config>
  <luceneMatchVersion>LUCENE_46</luceneMatchVersion>

  <!-- the standard directory factory should pick MMap for linux -->
  <!-- however since we're running on one platform, force it in that direction -->
  <directoryFactory name="DirectoryFactory"
                    class="${solr.directoryFactory:solr.MMapDirectoryFactory}"/>

  <indexConfig>
    <!-- compound file format means fewer open files on the OS, but does have a small performance cost -->
    <useCompoundFile>false</useCompoundFile>

    <!-- maximum number of segments before solr starts to combine them -->
    <!-- more segments means faster indexing, fewer means faster searching -->
    <!-- since these segments are shipped directly to the replicas we have to keep the number -->
    <!-- low even on the master -->
    <mergeFactor>5</mergeFactor>
    <!-- Sets the amount of RAM that may be used by Lucene indexing -->
    <!-- for buffering added documents and deletions before they are -->
    <!-- flushed to the Directory. We'd need to experiment with patient documents to find an optimal number -->
    <!-- to keep in memory before a commit is triggered  -->
    <ramBufferSizeMB>32</ramBufferSizeMB>

    <!-- the maximum field length, 10 million characters in our case is far more than enough -->
    <filter class="solr.LimitTokenCountFilterFactory" maxTokenCount="10000000"/>
    <writeLockTimeout>1000</writeLockTimeout>

    <lockType>native</lockType>
  </indexConfig>

  <!-- enable jmx, uses config through the jvm -->
  <jmx />

  <!-- The default high-performance update handler -->
  <updateHandler class="solr.DirectUpdateHandler2">
    <autoSoftCommit>
      <!-- 10 seconds -->
      <maxTime>10000</maxTime>
    </autoSoftCommit>
  </updateHandler>

  <requestHandler name="/search" class="solr.SearchHandler" default="true">
    <arr name="components">
      <!-- only allow the query and debug components to be used -->
      <str>query</str>
      <str>debug</str>
      <str>facet</str>
    </arr>
    <!-- default to the enhanced dismax query parser -->
    <!-- this allows us to default to two different name fields -->
    <lst name="defaults">
      <str name="defType">edismax</str>
      <str name="qf">condition^12.0 condition_starts_with^15.0 city state zip title^18.0 title_starts_with^16.0 sitename^8.0 sitename_starts_with^10.0 address address_starts_with</str>
      <!-- set minimum match to zero to disable the feature -->
      <str name="mm">0</str>
    </lst>
  </requestHandler>
  <requestHandler name="/update" class="solr.UpdateRequestHandler" />
  <requestHandler name="/update/javabin" class="solr.UpdateRequestHandler" />
  <requestHandler name="/update/json" class="solr.JsonUpdateRequestHandler" startup="lazy" />
  <requestHandler name="/admin/" class="solr.admin.AdminHandlers" />
  <requestHandler name="/analysis/field" class="solr.FieldAnalysisRequestHandler" />

  <!-- ping/healthcheck -->
  <requestHandler name="/admin/ping" class="solr.PingRequestHandler">
    <lst name="defaults">
      <str name="qt">search</str>
      <str name="q">solrpingquery</str>
      <str name="echoParams">all</str>
      <str name="df">trial_id_key</str>
    </lst>
  </requestHandler>

  <query>
    <maxBooleanClauses>4096</maxBooleanClauses>

    <!-- Depending on hit rates some caches should be switched to FastLRU caches -->
    <!-- the inverse is also true, LRUCache is usually better for hit rates under 75% -->

    <!-- filter caches are very useful for filter queries (most likely tenant based filters) -->
    <!-- and potentially for dates where you're not really looking to score but rather limit results -->
    <!-- this should be set near the max number of tenants for a powerworks client -->
    <!-- in some cases solr will cache raw query results here as well -->
    <!-- entry size flexes based on number of records in the index and how dense the docset is -->
    <!-- this can get very large -->
    <filterCache class="solr.FastLRUCache" size="1000" initialSize="200" autowarmCount="20"/>

    <!-- Sorted response to a particular query/limit/start combo -->
    <!-- it's difficult to tell if we'll get many hits here, this will largely be dominated -->
    <!-- by user query behavior. Unneeded autowarming can also be very expensive -->
    <queryResultCache class="solr.LRUCache" size="1000" initialSize="500" autowarmCount="0"/>

    <!-- Document Cache for stored values, we have very few so entries will be small -->
    <!-- must be greater than limit * max concurrent queries to revent in flight refetching -->
    <!-- docIds are transient so autowarming is unneeded -->
    <documentCache class="solr.LRUCache" size="5000" initialSize="1000" autowarmCount="0"/>

    <!-- field name to docId/field term ID pairs -->
    <!-- this is primarily used for faceting and will likely be useless for us -->
    <fieldValueCache class="solr.LRUCache" size="10" autowarmCount="0" showItems="10" />

    <enableLazyFieldLoading>true</enableLazyFieldLoading>

    <!-- This should flex based on our limits and expected paging amounts -->
    <!-- limit = 25, expected number of pages we want to cache = 3, max doc = 75 -->
    <!-- set to 200 until we know better -->
    <queryResultWindowSize>200</queryResultWindowSize>

    <!-- this should always be greater than what we expect our limit to be -->
    <queryResultMaxDocsCached>200</queryResultMaxDocsCached>

    <!-- if no searcher is ready use a cold one to do searches immediately -->
    <useColdSearcher>true</useColdSearcher>

    <!-- Replicas will require more resources to warm caches, limit the number of warming searchers -->
    <maxWarmingSearchers>2</maxWarmingSearchers>
  </query>

  <requestDispatcher handleSelect="true" >
    <!-- disable http caching -->
    <httpCaching never304="true" />
  </requestDispatcher>

  <!-- config for the admin interface -->
  <admin>
    <defaultQuery>solr</defaultQuery>
  </admin>
</config> 
